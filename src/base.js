import Rebase from "re-base";
import firebase from 'firebase/app';
import 'firebase/database';

const FirebaseApp = firebase.initializeApp({
  apiKey: "AIzaSyAD5wjLAVo4SZZhEfivNh-ZNnUGFvEa9ss",
  authDomain: "dotheap-development.firebaseapp.com",
  databaseURL: "https://dotheap-development.firebaseio.com"
});

const base = Rebase.createClass(firebase.database());

export { FirebaseApp };

export default base;
