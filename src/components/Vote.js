import React from "react";
import firebase from "firebase/app";
import { Container, List, Segment, Button, Icon } from "semantic-ui-react";
import base from "../base.js";
import Login from "./Login.js";

class Vote extends React.Component {
  state = {
    userId: null,
    userEmail: null,
    items: {},
    listName: null,
    votesPerUser: null
  };

  componentDidMount() {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.authHandler({ user });
      }

      this.ref = base.syncState(
        "lists/" + this.props.match.params.listId + "/items",
        {
          context: this,
          state: "items"
        }
      );

      this.ref = base.syncState(
        "lists/" + this.props.match.params.listId + "/name",
        {
          context: this,
          state: "listName"
        }
      );

      this.ref = base.syncState(
        "lists/" + this.props.match.params.listId + "/votesPerUser",
        {
          context: this,
          state: "votesPerUser"
        }
      );
    });
  }

  componentWillUnmount() {
    base.removeBinding(this.ref);
  }

  authHandler = async authData => {
    this.setState({
      userId: authData.user.uid,
      userEmail: authData.user.email
    });
  };

  logout = async () => {
    await firebase.auth().signOut();
    this.setState({
      userId: null
    });
  };

  votesForItem = key => {
    var item = this.state.items[key];
    if (item) {
      var votes = item["votes-per-user"];
      if (votes) {
        return Object.keys(votes).map(userKey => {
          var userVotes = votes[userKey];
          return userVotes;
        });
      }
    }
  };

  totalVotesForItem = key => {
    var votesForItem = this.votesForItem(key);
    if (votesForItem) {
      return votesForItem.reduce((accumulator, currentValue) => {
        return accumulator + Object.keys(currentValue).length;
      }, 0);
    }
  };

  userVotesForItem = key => {
    var item = this.state.items[key];
    var votes = item["votes-per-user"];
    if (votes) {
      var userVotes = votes[[this.state.userId]];
      if (userVotes) {
        return userVotes;
      }
    }
  };

  totalUserVotesForItem = key => {
    var userVotes = this.userVotesForItem(key);
    return userVotes ? Object.keys(userVotes).length : 0;
  };

  userVotes = () => {
    // Easier with a seperate Firebase endpoint
    return Object.keys(this.state.items).map(key => {
      var userVotes = this.userVotesForItem(key);
      if (userVotes) {
        return userVotes;
      } else {
        return {};
      }
    });
  };

  totalUserVotes = () => {
    return this.userVotes().reduce((accumulator, currentValue) => {
      return accumulator + Object.keys(currentValue).length;
    }, 0);
  };

  totalVotesForOthers = key => {
    return this.totalVotesForItem(key) - (this.totalUserVotesForItem(key) || 0);
  };

  addVote = key => {
    if (this.totalUserVotes() < this.state.votesPerUser) {
      base.push(
        `lists/${this.props.match.params.listId}/items/${key}/votes-per-user/${
          this.state.userId
        }/`,
        {
          data: true
        }
      );
    }
  };

  removeVote = key => {
    var userVotes = this.userVotesForItem(key);
    if (userVotes) {
      var lastVoteKey = Object.keys(userVotes)[
        Object.keys(userVotes).length - 1
      ];
      base.remove(
        `lists/${this.props.match.params.listId}/items/${key}/votes-per-user/${
          this.state.userId
        }/${lastVoteKey}`
      );
    }
  };

  renderDots = (n, color = "black", noDots = null) => {
    var votes = [];
    for (let index = 0; index < n; index++) {
      votes.push(<Icon key={index} name="circle" color={color} />);
    }
    if (noDots && votes.length === 0) {
      votes.push(<span key={0}>{noDots}</span>);
    }
    return votes;
  };

  render() {
    const logout = (
      <Button onClick={this.logout}>Log out ({this.state.userEmail})</Button>
    );

    if (!this.state.userId) {
      return <Login authHandler={this.authHandler} />;
    } else {
      return (
        <div>
          <Container>
            <div className="menu">{logout}</div>
            <h1>{this.state.listName}</h1>
            <Segment>
              {this.renderDots(
                this.state.votesPerUser - this.totalUserVotes(),
                "blue",
                "Thanks for voting! 🚀"
              )}
            </Segment>
            <Segment>
              <List relaxed>
                {Object.keys(this.state.items).map(key => {
                  return (
                    <List.Item key={key}>
                      <List.Icon name="caret right" />
                      <List.Content>
                        <span
                          onClick={() => {
                            this.addVote(key);
                          }}
                        >
                          {this.state.items[key].task}
                        </span>
                        {this.renderDots(this.totalVotesForOthers(key), "grey")}{" "}
                        {this.renderDots(
                          this.totalUserVotesForItem(key),
                          "blue"
                        )}
                        <button
                          onClick={() => {
                            this.addVote(key);
                          }}
                        >
                          +
                        </button>
                        <button
                          onClick={() => {
                            this.removeVote(key);
                          }}
                        >
                          -
                        </button>
                      </List.Content>
                    </List.Item>
                  );
                })}
              </List>
            </Segment>
          </Container>
        </div>
      );
    }
  }
}

export default Vote;
