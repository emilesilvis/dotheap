import React from "react";
import { List, Input } from "semantic-ui-react";

class Items extends React.Component {
  makeItemEditable = key => {
    let updatedItem = this.props.items[key];
    updatedItem.inEdit = true;
    this.props.updateItem(key, updatedItem);
  };

  makeItemUneditable = key => {
    let updatedItem = this.props.items[key];
    updatedItem.inEdit = false;
    this.props.updateItem(key, updatedItem);
  };

  editItemTask = (event, key) => {
    let updatedItem = this.props.items[key];
    updatedItem.task = event.target.value;
    this.props.updateItem(key, updatedItem);
  };

  handleEnterKeyPress = (key, event) => {
    if (event.key === "Enter") {
      this.makeItemUneditable(key);
    }
  };

  renderItem = key => {
    let item = this.props.items[key];
    if (item.inEdit) {
      return (
        <List.Item key={key}>
          <Input
            type="text"
            name="name"
            // ref={this.taskRef}
            value={item.task}
            onChange={(event) => {
              this.editItemTask(event, key);
            }}
            onKeyPress={event => {
              this.handleEnterKeyPress(key, event);
            }}
          />
        </List.Item>
      );
    } else {
      return (
        <List.Item key={key} onDoubleClick={() => this.makeItemEditable(key)}>
          <List.Icon name="caret right" />
          <List.Content>
            {item.task}{" "}
            <button
              onClick={() => {
                this.props.removeItem(key);
              }}
            >
              x
            </button>
          </List.Content>
        </List.Item>
      );
    }
  };

  render() {
    return (
      <div>
        <List relaxed>
          {Object.keys(this.props.items).map(this.renderItem)}
        </List>
      </div>
    );
  }
}

export default Items;
