import React from "react";
import firebase from "firebase/app";
import base from "../base.js";
import {
  Container,
  Button,
  Header,
  Form,
  Input,
  Segment
} from "semantic-ui-react";
import Login from "./Login.js";
import Items from "./Items.js";

class Survey extends React.Component {
  taskRef = React.createRef();

  state = {
    userId: null,
    listId: null,
    listName: null,
    items: {},
    titleInEdit: false
  };

  componentDidMount() {
    this.setState({
      listId: this.props.match.params.listId
    });

    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.authHandler({ user });
      }
    });

    this.ref = base.syncState(
      "users/" +
        this.props.match.params.userId +
        "/lists/" +
        this.props.match.params.listId +
        "/name",
      {
        context: this,
        state: "listName",
        defaultValue: "New Survey"
      }
    );

    this.ref = base.syncState(
      "lists/" + this.props.match.params.listId + "/name",
      {
        context: this,
        state: "listName",
        defaultValue: "New Survey"
      }
    );

    this.ref = base.syncState(
      "users/" +
        this.props.match.params.userId +
        "/lists/" +
        this.props.match.params.listId +
        "/items",
      {
        context: this,
        state: "items"
      }
    );

    this.ref = base.syncState(
      "lists/" + this.props.match.params.listId + "/items",
      {
        context: this,
        state: "items"
      }
    );
  }

  componentWillUnmount() {
    base.removeBinding(this.ref);
  }

  authHandler = async authData => {
    if (authData.user.uid === this.props.match.params.userId) {
      this.setState({
        userId: authData.user.uid,
        userEmail: authData.user.email
      });
    }
  };

  logout = async () => {
    await firebase.auth().signOut();
    this.setState({
      userId: null
    });
  };

  addItem = event => {
    event.preventDefault();

    let task = this.taskRef.current.value;

    if (task !== "") {
      const item = {
        task: this.taskRef.current.value,
        inEdit: false,
        votes: {}
      };

      const items = { ...this.state.items };

      items["item_" + Date.now()] = item;

      this.setState({
        items: items
      });
    }

    event.currentTarget.reset();
  };

  updateItem = (key, updatedItem) => {
    const items = { ...this.state.items };

    items[key] = updatedItem;

    this.setState({
      items: items
    });
  };

  removeItem = key => {
    const items = { ...this.state.items };

    items[key] = null;

    this.setState({
      items: items
    });
  };

  deleteList = () => {
    if (window.confirm("delete " + this.state.listId + "?")) {
      base
        .remove(
          "users/" +
            this.props.match.params.userId +
            "/lists/" +
            this.props.match.params.listId
        )
        .then(() => {
          this.props.history.push("/");
        });
    }
  };

  makeListNameEditable = () => {
    this.setState({
      titleInEdit: true
    });
  };

  makeListNameUneditable = () => {
    this.setState({
      titleInEdit: false
    });
  };

  editListName = event => {
    this.setState({
      listName: event.target.value
    });
  };

  handleEnterKeyPress = event => {
    if (event.key === "Enter") {
      this.makeListNameUneditable();
    }
  };

  render() {
    const logout = (
      <Button onClick={this.logout}>Log out ({this.state.userEmail})</Button>
    );

    if (!this.state.userId) {
      return <Login authHandler={this.authHandler} />;
    } else {
      const listVoteUrl =
        "https://sleepy-kare-493490.netlify.com/lists/" +
        this.state.listId +
        "/vote";
      const listName = () => {
        if (this.state.titleInEdit) {
          return (
            <Input
              type="text"
              name="listName"
              value={this.state.listName}
              onChange={this.editListName.bind(this)}
              onKeyPress={this.handleEnterKeyPress}
            />
          );
        } else {
          return (
            <Header as="h1" onDoubleClick={this.makeListNameEditable}>
              {this.state.listName}
            </Header>
          );
        }
      };
      return (
        <div>
          <Container>
          <div className="menu">
            <Button onClick={this.deleteList}>Delete Survey</Button>
            {logout}
          </div>
            <Header>{listName()}</Header>
            <Segment color="green">
              Voting URL: <a href={listVoteUrl}>{listVoteUrl}</a>
            </Segment>
            <Segment>
              <Items
                items={this.state.items}
                updateItem={this.updateItem}
                removeItem={this.removeItem}
              />
              <Form onSubmit={this.addItem}>
                <Form.Field className="topic-add-input">
                  <input
                    name="task"
                    ref={this.taskRef}
                    type="text"
                    placeholder="Add topic"
                  />
                </Form.Field>
              </Form>
            </Segment>
          </Container>
        </div>
      );
    }
  }
}

export default Survey;
