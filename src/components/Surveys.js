import React from "react";
import firebase from "firebase/app";
import base from "../base.js";
import Login from "./Login.js";
import { Container, Button, List, Segment } from "semantic-ui-react";

class Surveys extends React.Component {
  state = {
    lists: {}
  };

  componentDidMount() {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.authHandler({ user });
      }

      this.ref = base.syncState("users/" + this.state.userId + "/lists", {
        context: this,
        state: "lists"
      });
    });
  }

  componentWillUnmount() {
    base.removeBinding(this.ref);
  }

  authHandler = async authData => {
    this.setState({
      userId: authData.user.uid,
      userEmail: authData.user.email
    });
  };

  logout = async () => {
    await firebase.auth().signOut();
    this.setState({
      userId: null
    });
  };

  newList = async () => {
    var newListData = {
      name: "New Survey",
      ownerUserId: this.state.userId,
      votesPerUser: 5
    };
    base
      .push("users/" + this.state.userId + "/lists/", {
        data: newListData
      })
      .then(ref => {
        base
          .post("lists/" + ref.key, {
            data: newListData
          })
          .then(() => {
            this.props.history.push(
              "/users/" + this.state.userId + "/lists/" + ref.key
            );
          });
      });
  };

  handleListLinkClick = key => {
    this.props.history.push("/users/" + this.state.userId + "/lists/" + key);
  };

  render() {
    const logout = (
      <Button onClick={this.logout}>Log out ({this.state.userEmail})</Button>
    );

    if (!this.state.userId) {
      return <Login authHandler={this.authHandler} />;
    } else {
      return (
        <Container>
           <div className="menu">
            <Button onClick={this.newList}>New Survey</Button>
            {logout}
          </div>
          <Segment>
            <List divided relaxed>
              {Object.keys(this.state.lists).map(key => {
                return (
                  <List.Item key={key}>
                    <List.Icon name="caret right" />
                    <List.Content>
                      <a
                        href=""
                        onClick={() => {
                          this.handleListLinkClick(key);
                        }}
                      >
                        {this.state.lists[key].name}
                      </a>
                    </List.Content>
                  </List.Item>
                );
              })}
            </List>
          </Segment>
        </Container>
      );
    }
  }
}

export default Surveys;
