import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Surveys from "./Surveys";
import Survey from "./Survey";
import Vote from "./Vote";
import NotFound from "./NotFound.js";

const Router = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={Surveys} />
      <Route path="/users/:userId/lists/:listId" component={Survey} />
      <Route path="/lists/:listId/vote" component={Vote} />
      <Route component={NotFound} />
    </Switch>
  </BrowserRouter>
);

export default Router;
