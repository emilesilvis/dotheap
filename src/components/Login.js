import React from "react";
import firebase from "firebase/app";
import "firebase/auth";
import { FirebaseApp } from "../base.js";
import { Container, Header, Button, Segment } from "semantic-ui-react";

class Login extends React.Component {
  authenticate = event => {
    const googleAuthProvider = new firebase.auth["GoogleAuthProvider"]();
    FirebaseApp.auth()
      .signInWithPopup(googleAuthProvider)
      .then(this.props.authHandler);
  };

  render() {
    return (
      <Container>
        <Segment>
          <Header as="h1" textAlign="center">
            Login
          </Header>
          <Button
            color="google plus"
            onClick={() => {
              this.authenticate();
            }}
          >
            Google
          </Button>
        </Segment>
      </Container>
    );
  }
}

export default Login;
