import React from "react";
import ReactDOM from "react-dom";
import Router from "./components/Router.js";
import 'semantic-ui-css/semantic.min.css';
import "./index.css";
  
var destination = document.querySelector("#container");
  
ReactDOM.render(
    <div>
      <Router />
    </div>,
    destination
);