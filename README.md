# Dotheap

A small [dot voting](https://en.wikipedia.org/wiki/Dot-voting) web app built using React and Firebase. Check out [a demo](https://sleepy-kare-493490.netlify.com/).

## Screenshots

### Create a survey

![Alt text](https://monosnap.com/image/D6RKwYooq3sAlrFmFLKNMc07HBuptF.png)

### Send out the survey for voting

![Alt text](https://monosnap.com/image/gxIQN0LQRfMPd4AEqXvdCUkrvCZZOQ.png)
